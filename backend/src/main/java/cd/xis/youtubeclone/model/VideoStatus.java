package cd.xis.youtubeclone.model;

public enum VideoStatus {
    PUBLIC, PRIVATE, UNLISTED
}
