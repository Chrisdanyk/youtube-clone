package cd.xis.youtubeclone.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import cd.xis.youtubeclone.model.Video;

public interface VideoRepository extends MongoRepository<Video, String> {

}