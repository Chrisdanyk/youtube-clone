package cd.xis.youtubeclone.service;

import cd.xis.youtubeclone.dto.CommentDto;
import cd.xis.youtubeclone.dto.UploadVideoResponse;
import cd.xis.youtubeclone.dto.VideoDto;
import cd.xis.youtubeclone.model.Comment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cd.xis.youtubeclone.model.Video;
import cd.xis.youtubeclone.repository.VideoRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VideoService {
    private final S3Service s3Service;
    private final VideoRepository videoRepository;
    private final UserService userService;

    public UploadVideoResponse uploadVideo(MultipartFile file) {
        var videoUrl = this.getVideoUrl(file);
        var video = new Video();
        video.setVideoUrl(videoUrl);
        var savedVideo = videoRepository.save(video);
        return new UploadVideoResponse(savedVideo.getId(), savedVideo.getVideoUrl());
    }

    public String uploadThumbnail(MultipartFile file, String videoId) {
        var savedVideo = this.getVideoById(videoId);
        var thumbnailUrl = s3Service.uploadFile(file);
        savedVideo.setThumbnailUrl(thumbnailUrl);
        videoRepository.save(savedVideo);
        return thumbnailUrl;
    }

    public VideoDto editVideo(VideoDto videoDto) {
        var savedVideo = videoRepository.findById(videoDto.getId()).orElseThrow(() -> new IllegalArgumentException("Video not found for id: " + videoDto.getId()));
        savedVideo.setTitle(videoDto.getTitle());
        savedVideo.setDescription(videoDto.getDescription());
        savedVideo.setTags(videoDto.getTags());
        savedVideo.setThumbnailUrl(videoDto.getThumbnailUrl());
        savedVideo.setVideoStatus(videoDto.getVideoStatus());
        videoRepository.save(savedVideo);
        return videoDto;
    }

    public String getVideoUrl(MultipartFile file) {
        return s3Service.uploadFile(file);
    }

    public Video getVideoById(String videoId) {
        return videoRepository.findById(videoId).orElseThrow(() -> new IllegalArgumentException("Video not found !!"));
    }

    public VideoDto getVideoDetails(String videoId) {
        Video savedVideo = this.getVideoById(videoId);
        boolean contains = userService.getCurrentUser().getVideoHistory().stream().anyMatch(s -> s.contains(savedVideo.getId()));
        if (!contains) {
            increaseVideoCount(savedVideo);
            userService.addVideoToHistory(videoId);

        }
        return mapToVideoDto(savedVideo);
    }

    private void increaseVideoCount(Video savedVideo) {
        savedVideo.incrementVideoCount();
        videoRepository.save(savedVideo);
    }

    public VideoDto likeVideo(String videoId) {
        Video videoById = this.getVideoById(videoId);
        if (userService.ifLikedVideo(videoId)) {
            videoById.decrementLikes();
            userService.removeFromLikedVideos(videoId);
        } else if (userService.ifDislikedVideo(videoId)) {
            videoById.decrementDisLikes();
            userService.removeFromDisLikedVideos(videoId);
            videoById.incrementLikes();
            userService.addToLikedVideos(videoId);
        } else {
            videoById.incrementLikes();
            userService.addToLikedVideos(videoId);
        }

        videoRepository.save(videoById);
        return mapToVideoDto(videoById);
    }

    public VideoDto dislikeVideo(String videoId) {
        Video videoById = this.getVideoById(videoId);
        if (userService.ifDislikedVideo(videoId)) {
            videoById.decrementDisLikes();
            userService.removeFromDisLikedVideos(videoId);
        } else if (userService.ifLikedVideo(videoId)) {
            videoById.decrementLikes();
            userService.removeFromLikedVideos(videoId);
            videoById.incrementDisLikes();
            userService.addToDisLikedVideos(videoId);
        } else {
            videoById.incrementDisLikes();
            userService.addToDisLikedVideos(videoId);
        }

        videoRepository.save(videoById);
        return mapToVideoDto(videoById);
    }

    private VideoDto mapToVideoDto(Video videoById) {
        VideoDto videoDto = new VideoDto();
        videoDto.setId(videoById.getId());
        videoDto.setTitle(videoById.getTitle());
        videoDto.setDescription(videoById.getDescription());
        videoDto.setVideoStatus(videoById.getVideoStatus());
        videoDto.setTags(videoById.getTags());
        videoDto.setVideourl(videoById.getVideoUrl());
        videoDto.setThumbnailUrl(videoById.getThumbnailUrl());
        videoDto.setLikeCount(videoById.getLikes().get());
        videoDto.setDislikeCount(videoById.getDisLikes().get());
        videoDto.setViewCount(videoById.getViewCount().get());
        videoDto.setVideoStatus(videoById.getVideoStatus());
        return videoDto;
    }


    public void addComment(String videoId, CommentDto commentDto) {
        Video video = this.getVideoById(videoId);
        Comment comment = new Comment();
        comment.setText(commentDto.getCommentText());
        comment.setAuthor(commentDto.getAuthorId());
        video.addComment(comment);
        videoRepository.save(video);
    }

    public List<CommentDto> getAllComments(String videoId) {
        Video video = this.getVideoById(videoId);
        return video.getComments().stream().map(this::mapToCommentDto).toList();

    }

    private CommentDto mapToCommentDto(Comment comment) {
        CommentDto commentDto = new CommentDto();
        commentDto.setCommentText(comment.getText());
        commentDto.setAuthorId(comment.getAuthor());
        return commentDto;
    }

    public List<VideoDto> getAllVideos() {
        return videoRepository.findAll().stream().map(this::mapToVideoDto).collect(Collectors.toList());
    }
}
