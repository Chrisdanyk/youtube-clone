package cd.xis.youtubeclone.controller;

import cd.xis.youtubeclone.service.UserRegistrationService;
import cd.xis.youtubeclone.service.UserService;
import com.nimbusds.jwt.JWT;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserRegistrationService userRegistrationService;
    private final UserService userService;

    @GetMapping("/register")
    @ResponseStatus(OK)
    public String register(Authentication authentication) {
        Jwt jwt = (Jwt) authentication.getPrincipal();
        return userRegistrationService.registerUser(jwt.getTokenValue());
    }

    @PostMapping("subscribe/{userId}")
    @ResponseStatus(OK)
    public boolean subscribeUser(@PathVariable("userId") String userId) {
        userService.subscribeUser(userId);
        return true;
    }

    @PostMapping("unsubscribe/{userId}")
    @ResponseStatus(OK)
    public boolean unSubscribeUser(@PathVariable("userId") String userId) {
        userService.unSubscribeUser(userId);
        return true;
    }

    @GetMapping("{userId}/history")
    @ResponseStatus(OK)
    public Set<String> userHistory(@PathVariable("userId") String userId) {
        return userService.userHistory(userId);

    }
}
