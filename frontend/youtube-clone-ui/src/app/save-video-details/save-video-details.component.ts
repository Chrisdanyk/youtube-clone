import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {MatChipInputEvent} from "@angular/material/chips";
import {ActivatedRoute} from "@angular/router";
import {VideoService} from "../video.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {VideoDto} from "../video-dto";

export interface Fruit {
  name: string;
}

@Component({
  selector: 'app-save-video-details',
  templateUrl: './save-video-details.component.html',
  styleUrls: ['./save-video-details.component.css'],
})

export class SaveVideoDetailsComponent implements OnInit {
  saveVideoDetailsForm: FormGroup;
  title: FormControl = new FormControl('');
  description: FormControl = new FormControl('');
  videoStatus: FormControl = new FormControl('');
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  tags: string[] = [];
  selectedFile !: File;
  selectedFileName = '';
  fileSelected = false;
  videoId = '';
  videoUrl!: string;
  thumbnailUrl!: string;
  likeCount: number = 0;
  dislikeCount: number = 0;
  viewCount: number = 0;


  constructor(private activatedRoute: ActivatedRoute, private videoService: VideoService, private _snackBar: MatSnackBar) {
    this.videoId = this.activatedRoute.snapshot.params['videoId'];
    this.videoService.getVideo(this.videoId).subscribe(data => {
      this.videoUrl = data.videourl;
      this.thumbnailUrl = data.thumbnailUrl;
      this.tags = data.tags;
      this.likeCount = data.likeCount;
      this.dislikeCount = data.dislikeCount;
      this.viewCount = data.viewCount;
      this.saveVideoDetailsForm.patchValue(
        {
          title: data.title,
          description: data.description,
          videoStatus: data.VideoStatus,

        }
      )
    });
    this.saveVideoDetailsForm = new FormGroup(
      {
        title: this.title,
        description: this.description,
        videoStatus: this.videoStatus
      }
    )
  }


  ngOnInit(): void {

  }


  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.tags.push(value);
    }
    event.chipInput!.clear();
  }

  remove(value: string): void {
    const index = this.tags.indexOf(value);
    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  onFileSelected(event: Event) {
    //@ts-ignore
    this.selectedFile = event.target.files[0];
    this.selectedFileName = this.selectedFile.name;
    this.fileSelected = true;
  }

  onUpload() {
    this.videoService.uploadThumbnail(this.selectedFile, this.videoId).subscribe(data => {
      this.thumbnailUrl = data;
      this._snackBar.open("Thumbnail uploaded", "OK");
    });
  }


  saveVideo() {
    const videoMetaData: VideoDto = {
      "id": this.videoId,
      "title": this.saveVideoDetailsForm.get('title')?.value,
      "description": this.saveVideoDetailsForm.get('description')?.value,
      "tags": this.tags,
      "videourl": this.videoUrl,
      "VideoStatus": this.saveVideoDetailsForm.get('VideoStatus')?.value,
      "thumbnailUrl": this.thumbnailUrl,
      "likeCount": this.likeCount,
      "dislikeCount": this.dislikeCount,
      "viewCount": this.viewCount
    }
    this.videoService.saveVideo(videoMetaData).subscribe(data => {
      this._snackBar.open("Video Metadata Updated successfully", "OK");
    })

  }
}
