import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userId: string = '';

  constructor(private httpclient: HttpClient) {
  }


  subscribeToUser(userId: string): Observable<boolean> {
    return this.httpclient.post<boolean>("http://localhost:8080/api/user/subscribe/" + userId + "/", null);
  }

  registerUser() {
    this.httpclient.get("http://localhost:8080/api/user/register", {responseType: "text"})
      .subscribe(data => {
        this.userId = data;
      })
  }

  getUserId(): string {
    return this.userId;
  }

  UnSubscribeToUser(userId: string): Observable<boolean> {
    return this.httpclient.post<boolean>("http://localhost:8080/api/user/unsubscribe/" + userId + "/", null);
  }

  getUserSubscriptions(userId: string): Observable<string[]> {
    return this.httpclient.get<string[]>("http://localhost:8080/api/user/"+userId+"/subscriptions");
  }

  getUserHistory(userId: string): Observable<string[]> {
    return this.httpclient.get<string[]>("http://localhost:8080/api/user/"+userId +"/history");
  }
}
