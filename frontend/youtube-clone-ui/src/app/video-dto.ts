export interface VideoDto {
  id: string;
  title: string;
  description: string;
  tags: Array<string>;
  videourl: string;
  VideoStatus: string;
  thumbnailUrl: string;
  likeCount: number;
  dislikeCount: number;
  viewCount: number;
}
