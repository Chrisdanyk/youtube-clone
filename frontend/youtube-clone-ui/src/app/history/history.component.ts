import {Component, OnInit} from '@angular/core';
import {UserService} from "../user.service";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  userHistory: Array<string> = [];

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.getUserHistory();
  }

  getUserHistory() {
    this.userService.getUserHistory(this.userService.getUserId()).subscribe(
      data => {
        console.log("data" + data);
        this.userHistory = data;

      }
    );
  }

}
