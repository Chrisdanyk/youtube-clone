import {NgModule} from '@angular/core';
import {AuthInterceptor, AuthModule} from 'angular-auth-oidc-client';
import {HTTP_INTERCEPTORS} from "@angular/common/http";


@NgModule({
  imports: [AuthModule.forRoot({
    config: {
      authority: 'https://dev-05zoqfk5.us.auth0.com',
      // redirectUrl: window.location.origin,
      redirectUrl: "http://localhost:4200/callback",
      clientId: 'fiDT8YUfpnaEnseZQcWLIi4Y8Rp66BOI',
      scope: 'openid profile offline_access email',
      responseType: 'code',
      silentRenew: true,
      useRefreshToken: true,
      secureRoutes: ['http://localhost:8080'],
      customParamsAuthRequest: {
        audience: 'http://localhost:8080'
      }
    }
  })],
  providers: [],
  exports: [AuthModule],
})
export class AuthConfigModule {
}
